package main

import (
	"fmt"
	"math/rand"
)

// List of cat sounds
var catSounds = []string{
	"meow",
	"purr",
	"hiss",
	"howl",
	"growl",
}

// main() calls printCatSound()
func main() {
	printCatSound()
}

// randomCatSound() selects a random cat sound from the list
func randomCatSound() string {
	selection := rand.Intn(len(catSounds))
	return catSounds[selection]
}

// printCatSound() prints out a cat sound
func printCatSound() {
	catSound := randomCatSound()
	fmt.Printf("%s", catSound)
}
