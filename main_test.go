package main

import (
	"testing"
)

// TestRandomCatSound() verifies that the cat sounds are selected
// from the required list
func TestRandomCatSound(t *testing.T) {
	soundExists := false

	selectedSound := randomCatSound()
	for _, sound := range catSounds {
		if selectedSound == sound {
			soundExists = true
		}
	}

	if !soundExists {
		t.Fatalf(`Selected sound '%s' does not exist.`, selectedSound)
	}
}
